var assert = require('assert');

describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function(){
      assert.equal(-1, [1,2,3].indexOf(4));
    });
  });
});

describe('Equality', function() {
  describe('simple addition', function() {
    it('should return that 1 plus 1 is equal to 2', function(){
      assert.equal(2, 1 + 1);
    });
  });
});

describe('Identity', function() {
  describe('student number', function() {
    it('should return student number of 117410434', function(){
      assert.equal(1, 1);
    });
  });
});